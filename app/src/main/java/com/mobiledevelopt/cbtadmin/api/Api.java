package com.mobiledevelopt.cbtadmin.api;


import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.M_delete_data;
import com.mobiledevelopt.cbtadmin.model.M_update_data;
import com.mobiledevelopt.cbtadmin.model.hasil.M_hasil;
import com.mobiledevelopt.cbtadmin.model.kuis.M_kuis;
import com.mobiledevelopt.cbtadmin.model.login.M_login;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.peserta.M_peserta;
import com.mobiledevelopt.cbtadmin.model.soal.M_soal;
import com.mobiledevelopt.cbtadmin.model.ujian.M_ujian;
import com.mobiledevelopt.cbtadmin.model.users.M_users;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    /*
    login user
     */
    @FormUrlEncoded
    @POST("login/user")
    Call<M_login> userLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("Users")
    Call<M_users> users();

    @FormUrlEncoded
    @POST("Users/add")
    Call<M_add_data> addUsers(
            @Field("nama") String nama,
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("Users/update")
    Call<M_update_data> updateUsers(
            @Field("nama") String nama,
            @Field("username") String username,
            @Field("password") String password,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("Users/delete")
    Call<M_delete_data> deleteUsers(
            @Field("id") String id
    );

    @GET("Soal")
    Call<M_kuis> kuis();

    @GET("MP")
    Call<M_mp> mp();

    @FormUrlEncoded
    @POST("MP/add")
    Call<M_add_data> addMp(
            @Field("nama") String nama
    );

    @FormUrlEncoded
    @POST("MP/update")
    Call<M_update_data> updateMp(
            @Field("nama") String nama,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("MP/delete")
    Call<M_delete_data> deleteMp(
            @Field("id") String id
    );

    @GET("Ujian")
    Call<M_ujian> ujian();

    @FormUrlEncoded
    @POST("Ujian/add")
    Call<M_add_data> addUjian(
            @Field("nama") String nama,
            @Field("tgl") String tgl,
            @Field("ket") String ket,
            @Field("waktu") String waktu
    );

    @FormUrlEncoded
    @POST("Ujian/hasil")
    Call<M_hasil> hasilUjian(
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("Ujian/update")
    Call<M_update_data> updateUjian(
            @Field("nama") String nama,
            @Field("tgl") String tgl,
            @Field("ket") String ket,
            @Field("waktu") String waktu,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("Ujian/delete")
    Call<M_delete_data> deleteUjian(
            @Field("id") String id
    );

    @GET("Peserta")
    Call<M_peserta> peserta(@Query("id_ujian") String path);

    @FormUrlEncoded
    @POST("Peserta/add")
    Call<M_add_data> addPeserta(
            @Field("nisn") String nisn,
            @Field("nama") String nama,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("alamat") String alamat,
            @Field("no_hp") String no_hp,
            @Field("ayah") String ayah,
            @Field("ibu") String ibu,
            @Field("wali") String wali,
            @Field("id_ujian") String id_ujian,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("Peserta/update")
    Call<M_update_data> updatePeserta(
            @Field("nisn") String nisn,
            @Field("nama") String nama,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("alamat") String alamat,
            @Field("no_hp") String no_hp,
            @Field("ayah") String ayah,
            @Field("ibu") String ibu,
            @Field("wali") String wali,
            @Field("id_ujian") String id_ujian,
            @Field("password") String password,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("Peserta/delete")
    Call<M_delete_data> deletePeserta(
            @Field("id") String id
    );

    @GET("Soal")
    Call<M_soal> soal(@Query("id_ujian") String path);

    @GET("Soal/bank_soal")
    Call<M_soal> bank_soal();

    @GET("Soal/editFilter")
    Call<M_soal> soalFilter(
            @Query("id") String path
    );

    @Multipart
    @POST("Soal/add")
    Call<M_add_data> addSoal(
            @Part MultipartBody.Part photo,
            @PartMap Map<String, RequestBody> text);

    @FormUrlEncoded
    @POST("Soal/delete")
    Call<M_delete_data> deleteSoal(
            @Field("id") String id
    );

    @Multipart
    @POST("Soal/update")
    Call<M_update_data> updateSoal(
            @Part MultipartBody.Part photo,
            @PartMap Map<String, RequestBody> text);
}
