package com.mobiledevelopt.cbtadmin.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.MPAdapter;
import com.mobiledevelopt.cbtadmin.adapter.UsersAdapter;
import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.users.M_data_users;
import com.mobiledevelopt.cbtadmin.model.users.M_users;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.mobiledevelopt.cbtadmin.util.Session_management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

import static com.mobiledevelopt.cbtadmin.api.Const.KEY_ID;

public class DataPelajaranActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog alertDialog;
    private RecyclerView recyclerView;
    private MPAdapter anggotaAdapter;
    private List<M_data_mp> list_anggota = new ArrayList<>();
    private HashMap<String, String> user;
    private LinearLayout ll_error,ll_loading,ll_no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_pelajaran);
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(DataPelajaranActivity.this);
        user = sessionManagement.getUserDetails();

        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);

        recyclerView = findViewById(R.id.recycleVIew);
        list_anggota = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                linearLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(anggotaAdapter);
        FloatingActionButton add = findViewById(R.id.add);

        getMp();
        add.setOnClickListener(this);
        reload.setOnClickListener(this);
    }

    private void getMp() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_mp> call = null;
        call = RetrofitClient.getInstance().getApi().mp();
        call.enqueue(new Callback<M_mp>() {
            @Override
            public void onResponse(Call<M_mp> call, retrofit2.Response<M_mp> response) {
                ll_loading.setVisibility(View.GONE);
                M_mp anggotaResponse = response.body();

                if (anggotaResponse.getData().size() > 0){
                    recyclerView.setVisibility(View.VISIBLE);
                    list_anggota = anggotaResponse.getData();
                    anggotaAdapter = new MPAdapter();
                    anggotaAdapter.addAnggota(list_anggota);
                    recyclerView.setAdapter(anggotaAdapter);
                }else{
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_mp> call, Throwable t) {
                Log.e("failur",t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                show_form_add();
                break;
            case R.id.btn_reload_rv:
                getMp();
                break;
        }
    }

    public void show_form_add() {
        final String emailPattern = getString(R.string.email_pattern);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_add_mp, null);

        final TextInputLayout til_nama = dialogView.findViewById(R.id.textInputLayoutNama);
        final TextInputEditText ed_nama = dialogView.findViewById(R.id.nama);
        final Button btn_add = dialogView.findViewById(R.id.btn_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
                        til_nama.setError("Nama Mata Pelajaran Kosong");
                    } else {
                        til_nama.setError(null);
                        add_mp(ed_nama.getText().toString().trim());
                    }
                }
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog =dialogBuilder.create();
        alertDialog.show();
    }

    private void add_mp(String nama) {
        M.showLoadingDialog(this);
        Call<M_add_data> call = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
            call = RetrofitClient.getInstance().getApi().addMp(nama);
        assert call != null;
        call.enqueue(new Callback<M_add_data>() {
            @Override
            public void onResponse(Call<M_add_data> call, retrofit2.Response<M_add_data> response) {
                M.hideLoadingDialog();
                M_add_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getStatus()){
                    new SweetAlertDialog(DataPelajaranActivity.this,SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                    alertDialog.dismiss();
                    ll_loading.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.GONE);
                    ll_error.setVisibility(View.GONE);
                    getMp();
                }else{
                    new SweetAlertDialog(DataPelajaranActivity.this,SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<M_add_data> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

}