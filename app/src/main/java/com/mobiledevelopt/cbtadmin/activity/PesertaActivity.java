package com.mobiledevelopt.cbtadmin.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.PesertaAdapter;
import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.kuis.M_kuis;
import com.mobiledevelopt.cbtadmin.model.kuis.M_kuis_data;
import com.mobiledevelopt.cbtadmin.model.peserta.M_data_peserta;
import com.mobiledevelopt.cbtadmin.model.peserta.M_peserta;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.mobiledevelopt.cbtadmin.util.Session_management;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class PesertaActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog alertDialog;
    private RecyclerView recyclerView;
    private PesertaAdapter pesertaAdapter;
    private List<M_data_peserta> list_anggota = new ArrayList<>();
    private HashMap<String, String> user;
    private LinearLayout ll_error, ll_loading, ll_no_data;
    private String id_ujian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peserta);
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        id_ujian= extras.getString("id");
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(PesertaActivity.this);
        user = sessionManagement.getUserDetails();

        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);

        recyclerView = findViewById(R.id.recycleVIew);
        list_anggota = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                linearLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pesertaAdapter);
        FloatingActionButton add = findViewById(R.id.add);

        getUsers();
        add.setOnClickListener(this);
        reload.setOnClickListener(this);
    }

    private void getUsers() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_peserta> call = null;
        call = RetrofitClient.getInstance().getApi().peserta(id_ujian);
        call.enqueue(new Callback<M_peserta>() {
            @Override
            public void onResponse(Call<M_peserta> call, retrofit2.Response<M_peserta> response) {
                ll_loading.setVisibility(View.GONE);
                M_peserta anggotaResponse = response.body();

                if (anggotaResponse.getData().size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    list_anggota = anggotaResponse.getData();
                    pesertaAdapter = new PesertaAdapter();
                    pesertaAdapter.addAnggota(list_anggota);
                    recyclerView.setAdapter(pesertaAdapter);
                } else {
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_peserta> call, Throwable t) {
                Log.e("failur", t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
//                show_form_add();
                show_form_choise();
                break;
            case R.id.btn_reload_rv:
                getUsers();
                break;
        }
    }

    private void get_kuis() {
        M.showLoadingDialog(this);
        Call<M_kuis> call = RetrofitClient.getInstance().getApi().kuis();
        assert call != null;
        call.enqueue(new Callback<M_kuis>() {
            @Override
            public void onResponse(Call<M_kuis> call, retrofit2.Response<M_kuis> response) {
                M.hideLoadingDialog();
                List<M_kuis_data> res_login = response.body().getData();
                Log.e("size kuis", String.valueOf(res_login.size()));

            }

            @Override
            public void onFailure(Call<M_kuis> call, Throwable t) {
                M.hideLoadingDialog();
                Log.e("log", t.getMessage());
                Toast.makeText(getApplicationContext(), "Masalah jaringan, coba lagi. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void show_form_choise() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_choise, null);

        final Button btn_add = dialogView.findViewById(R.id.btn_add_excel);
        final Button btn_add_by_one = dialogView.findViewById(R.id.btn_add_by_one);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PesertaActivity.this, WebViewActivity.class);
                startActivity(intent);
            }
        });
        btn_add_by_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_form_add();
            }
        });


        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void show_form_add() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_add_peserta, null);

        final TextInputLayout til_nisn = dialogView.findViewById(R.id.textInputLayoutNisn);
        final TextInputLayout til_nama = dialogView.findViewById(R.id.textInputLayoutNama);
        final TextInputLayout til_alamat = dialogView.findViewById(R.id.textInputLayoutAlamat);
        final TextInputLayout til_tgl_lahir = dialogView.findViewById(R.id.textInputLayoutTgl_lahir);
        final TextInputLayout til_no_hp = dialogView.findViewById(R.id.textInputLayoutNoHP);
        final TextInputLayout til_ayah = dialogView.findViewById(R.id.textInputLayoutAyah);
        final TextInputLayout til_ibu = dialogView.findViewById(R.id.textInputLayoutIbu);
        final TextInputLayout til_wali = dialogView.findViewById(R.id.textInputLayoutWali);
        final TextInputLayout til_password = dialogView.findViewById(R.id.textInputLayoutPassword);

        final TextInputEditText ed_nisn = dialogView.findViewById(R.id.ed_nisn);
        final TextInputEditText ed_nama = dialogView.findViewById(R.id.ed_nama);
        final TextInputEditText ed_alamat = dialogView.findViewById(R.id.ed_alamat);
        final TextInputEditText ed_tgl_lahir = dialogView.findViewById(R.id.ed_tgl_lahir);
        final TextInputEditText ed_no_hp = dialogView.findViewById(R.id.ed_no_hp);
        final TextInputEditText ed_ayah = dialogView.findViewById(R.id.ed_ayah);
        final TextInputEditText ed_ibu = dialogView.findViewById(R.id.ed_ibu);
        final TextInputEditText ed_wali = dialogView.findViewById(R.id.ed_wali);
        final TextInputEditText ed_password = dialogView.findViewById(R.id.ed_password);

        final Button btn_add = dialogView.findViewById(R.id.btn_add);
        final Button btn_select_date = dialogView.findViewById(R.id.btn_select_date);

        btn_select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(PesertaActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        ed_tgl_lahir.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_nisn.getText()).toString().isEmpty()) {
                        til_nisn.setError("NISN Kosong");
                        til_nisn.setError(null);
                        til_tgl_lahir.setError(null);
                        til_password.setError(null);
                    } else if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
                        til_nama.setError("Nama Kosong");
                        til_nama.setError(null);
                        til_tgl_lahir.setError(null);
                        til_password.setError(null);
                    } else if (Objects.requireNonNull(ed_tgl_lahir.getText()).toString().isEmpty()) {
                        til_tgl_lahir.setError("Tanggal Lahir Kosong");
                        til_nisn.setError(null);
                        til_nama.setError(null);
                        til_password.setError(null);
                    } else if (Objects.requireNonNull(ed_password.getText()).toString().isEmpty()) {
                        til_password.setError("Password Kosong");
                        til_nisn.setError(null);
                        til_tgl_lahir.setError(null);
                        til_nama.setError(null);
                    } else {
                        til_nama.setError(null);
                        add_users(ed_nisn.getText().toString().trim(), ed_nama.getText().toString().trim(), ed_tgl_lahir.getText().toString().trim(), ed_alamat.getText().toString().trim(), ed_no_hp.getText().toString().trim()
                                , ed_ayah.getText().toString().trim(), ed_ibu.getText().toString().trim(), ed_wali.getText().toString().trim(),id_ujian, ed_password.getText().toString().trim());
                    }
                }
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void add_users(String nisn, String nama, String tgl_lahir, String alamat,String no_hp,String ayah,String ibu,String wali,String id_ujian,String password) {
        M.showLoadingDialog(this);
        Call<M_add_data> call = null;
        call = RetrofitClient.getInstance().getApi().addPeserta(nisn, nama,tgl_lahir, alamat,no_hp,ayah,ibu,wali,id_ujian,password);
        assert call != null;
        call.enqueue(new Callback<M_add_data>() {
            @Override
            public void onResponse(Call<M_add_data> call, retrofit2.Response<M_add_data> response) {
                M.hideLoadingDialog();
                M_add_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getStatus()) {
                    new SweetAlertDialog(PesertaActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                    alertDialog.dismiss();
                    ll_loading.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.GONE);
                    ll_error.setVisibility(View.GONE);
                    getUsers();
                } else {
                    new SweetAlertDialog(PesertaActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<M_add_data> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

}