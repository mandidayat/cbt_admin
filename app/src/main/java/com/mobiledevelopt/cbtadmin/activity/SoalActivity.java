package com.mobiledevelopt.cbtadmin.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.SoalAdapter;
import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.soal.M_data_soal;
import com.mobiledevelopt.cbtadmin.model.soal.M_soal;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.mobiledevelopt.cbtadmin.util.Session_management;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.vincent.filepicker.Constant.*;

public class SoalActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog alertDialog;
    private RecyclerView recyclerView;
    private SoalAdapter pesertaAdapter;
    private List<M_data_soal> list_anggota = new ArrayList<>();
    private HashMap<String, String> user;
    private LinearLayout ll_error, ll_loading, ll_no_data;
    private String id_ujian,id_mapel;
    private List<M_data_mp> list_mp = new ArrayList<>();
    Spinner List_mp;
    private ImageView iv_soal;
    private Bitmap bitmap_soal;
    private File file;
    private ScrollView sv_content;
    private LinearLayout ll_loading_rv;
    private LinearLayout ll_error_rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal);
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        id_ujian= extras.getString("id");
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(SoalActivity.this);
        user = sessionManagement.getUserDetails();

        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);

        recyclerView = findViewById(R.id.recycleVIew);
        list_anggota = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                linearLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pesertaAdapter);
        FloatingActionButton add = findViewById(R.id.add);

        getSoal();
        add.setOnClickListener(this);
        reload.setOnClickListener(this);
    }

    private void getSoal() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_soal> call = null;
        call = RetrofitClient.getInstance().getApi().soal(id_ujian);
        call.enqueue(new Callback<M_soal>() {
            @Override
            public void onResponse(Call<M_soal> call, retrofit2.Response<M_soal> response) {
                ll_loading.setVisibility(View.GONE);
                M_soal anggotaResponse = response.body();
                Log.e("res",response.toString());
                if (anggotaResponse.getData().size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    list_anggota = anggotaResponse.getData();
                    pesertaAdapter = new SoalAdapter();
                    pesertaAdapter.addAnggota(list_anggota);
                    recyclerView.setAdapter(pesertaAdapter);
                } else {
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_soal> call, Throwable t) {
                Log.e("failur", t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
//                show_form_add();
                show_form_choise();
                break;
            case R.id.btn_reload_rv:
                getSoal();
                break;
        }
    }

    public void show_form_choise() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_choise, null);

        final Button btn_add = dialogView.findViewById(R.id.btn_add_excel);
        final TextView title = dialogView.findViewById(R.id.title);
        final Button btn_add_by_one = dialogView.findViewById(R.id.btn_add_by_one);
        title.setText("Add Soal");
        btn_add_by_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_form_add();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoalActivity.this, WebViewSoalActivity.class);
                startActivity(intent);
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void show_form_add() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_add_soal, null);

        final Spinner List_jawaban = dialogView.findViewById(R.id.spinner_jawaban);
        sv_content = dialogView.findViewById(R.id.sv_content);
        ll_loading_rv = dialogView.findViewById(R.id.ll_loading_rv);
        ll_error_rv = dialogView.findViewById(R.id.ll_error_rv);
        final Button btn_reload_rv = dialogView.findViewById(R.id.btn_reload_rv);
        btn_reload_rv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMp();
            }
        });
        List_mp = dialogView.findViewById(R.id.spinner_mp);
        final Button select_image = dialogView.findViewById(R.id.btn_select_img);
        iv_soal = dialogView.findViewById(R.id.iv_soal);

        file = null;
//        Toast.makeText(this, List_jawaban.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

        List_mp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                id_mapel = list_mp.get(position).getIdMapel();
                Toast.makeText(SoalActivity.this, list_mp.get(position).getMapel(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        final TextInputLayout til_soal = dialogView.findViewById(R.id.textInputLayoutSoal);
        final TextInputLayout til_pila = dialogView.findViewById(R.id.textInputLayoutPilA);
        final TextInputLayout til_pilb = dialogView.findViewById(R.id.textInputLayoutPilB);
        final TextInputLayout til_pilc = dialogView.findViewById(R.id.textInputLayoutPilC);
        final TextInputLayout til_pild = dialogView.findViewById(R.id.textInputLayoutPilD);

        final TextInputEditText ed_soal = dialogView.findViewById(R.id.ed_soal);
        final TextInputEditText ed_pila = dialogView.findViewById(R.id.ed_pila);
        final TextInputEditText ed_pilb = dialogView.findViewById(R.id.ed_pilb);
        final TextInputEditText ed_pilc = dialogView.findViewById(R.id.ed_pilc);
        final TextInputEditText ed_pild = dialogView.findViewById(R.id.ed_pild);

        final Button btn_add = dialogView.findViewById(R.id.btn_add);


        getMp();

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SoalActivity.this, ImagePickActivity.class);
                intent.putExtra(MAX_NUMBER,1);
                startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_soal.getText()).toString().isEmpty()) {
                        til_soal.setError("Soal Kosong");
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pila.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError("Pilihan A Kosong");
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pilb.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError("Pilihan B Kosong");
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pilc.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError("Pilihan C Kosong");
                        til_pild.setError(null);
                    }else if (Objects.requireNonNull(ed_pild.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError("Pilijan D Kosong");
                    } else {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                        add_users(ed_soal.getText().toString().trim(), ed_pila.getText().toString().trim(), ed_pilb.getText().toString().trim(), ed_pilc.getText().toString().trim(), ed_pild.getText().toString().trim()
                                , id_ujian, id_mapel,List_jawaban.getSelectedItem().toString());
                    }
                }
            }
        });


        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void getMp() {
        ll_loading_rv.setVisibility(View.VISIBLE);
        ll_error_rv.setVisibility(View.GONE);
        sv_content.setVisibility(View.GONE);
        Call<M_mp> call = null;
        call = RetrofitClient.getInstance().getApi().mp();
        call.enqueue(new Callback<M_mp>() {
            @Override
            public void onResponse(Call<M_mp> call, retrofit2.Response<M_mp> response) {
                M_mp anggotaResponse = response.body();
                if (anggotaResponse.getData().size() > 0){
                    ll_loading_rv.setVisibility(View.GONE);
                    ll_error_rv.setVisibility(View.GONE);
                    sv_content.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    list_mp = anggotaResponse.getData();
                    List<String> listSpinner = new ArrayList<>();
                    for (int i = 0; i < list_mp.size(); i++){
                        listSpinner.add(list_mp.get(i).getMapel());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(SoalActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, listSpinner);
                    List_mp.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<M_mp> call, Throwable t) {
                ll_loading_rv.setVisibility(View.GONE);
                ll_error_rv.setVisibility(View.VISIBLE);
                sv_content.setVisibility(View.GONE);
                Log.e("failur",t.getMessage());
            }
        });
    }

    private void add_users(String soal, String pila, String pilb, String pilc, String pild, String id_ujian, String id_mapel, String jawaban) {
        M.showLoadingDialog(this);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("id_ujian", createPartFromString(id_ujian));
        map.put("id_mapel", createPartFromString(id_mapel));
        map.put("soal", createPartFromString(soal));
        if(jawaban.equals("A")){
            map.put("jawaban", createPartFromString("1"));
        }else if(jawaban.equals("B")){
            map.put("jawaban", createPartFromString("2"));
        }else if(jawaban.equals("C")){
            map.put("jawaban", createPartFromString("3"));
        }else if(jawaban.equals("D")){
            map.put("jawaban", createPartFromString("4"));
        }
        map.put("pil1", createPartFromString(pila));
        map.put("pil2", createPartFromString(pilb));
        map.put("pil3", createPartFromString(pilc));
        map.put("pil4", createPartFromString(pild));
        //convert gambar jadi File terlebih dahulu dengan memanggil createTempFile yang di atas tadi.
//        File file = createTempFile(bitmap_soal);
        MultipartBody.Part body;

        if (file != null){
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
             body = MultipartBody.Part.createFormData("gambar", file.getName(), reqFile);
        }else{
            body = null;
        }

        Call<M_add_data> call = null;
        call = RetrofitClient.getInstance().getApi().addSoal(body,map);
        assert call != null;
        call.enqueue(new Callback<M_add_data>() {
            @Override
            public void onResponse(Call<M_add_data> call, retrofit2.Response<M_add_data> response) {
                M.hideLoadingDialog();
                M_add_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getStatus()) {
                    new SweetAlertDialog(SoalActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                    alertDialog.dismiss();
                    ll_loading.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.GONE);
                    ll_error.setVisibility(View.GONE);
                    getSoal();
                } else {
                    new SweetAlertDialog(SoalActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<M_add_data> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK && data != null){
            //TODO: action
            ArrayList<ImageFile> pickedImg = data.getParcelableArrayListExtra(RESULT_PICK_IMAGE);
            StringBuilder builder = new StringBuilder();
            for (ImageFile file : pickedImg) {
                String path = file.getPath();
                builder.append(path);
            }

            file = new File(builder.toString());
            Toast.makeText(this, file.getName(), Toast.LENGTH_SHORT).show();
//            Glide.with(this).load(builder.toString()).into(iv_soal);

            Glide.with(this)
                    .asBitmap()
                    .load(builder.toString())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            bitmap_soal = resource;
                            iv_soal.setImageBitmap(resource);
                        }
                    });

        }
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    private File createTempFile(Bitmap bitmap) {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , System.currentTimeMillis() +"_image.webp");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.WEBP,0, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}