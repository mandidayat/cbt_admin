package com.mobiledevelopt.cbtadmin.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.MPAdapter;
import com.mobiledevelopt.cbtadmin.adapter.UjianAdapter;
import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.ujian.M_data_ujian;
import com.mobiledevelopt.cbtadmin.model.ujian.M_ujian;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.mobiledevelopt.cbtadmin.util.Session_management;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class DataUjianActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog alertDialog;
    private RecyclerView recyclerView;
    private UjianAdapter anggotaAdapter;
    private List<M_data_ujian> list_anggota = new ArrayList<>();
    private HashMap<String, String> user;
    private LinearLayout ll_error,ll_loading,ll_no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_ujian);
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(DataUjianActivity.this);
        user = sessionManagement.getUserDetails();

        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);

        recyclerView = findViewById(R.id.recycleVIew);
        list_anggota = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
//                linearLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(anggotaAdapter);
        FloatingActionButton add = findViewById(R.id.add);

        getUjian();
        add.setOnClickListener(this);
        reload.setOnClickListener(this);
    }

    public void getUjian() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_ujian> call = null;
        call = RetrofitClient.getInstance().getApi().ujian();
        call.enqueue(new Callback<M_ujian>() {
            @Override
            public void onResponse(Call<M_ujian> call, retrofit2.Response<M_ujian> response) {
                ll_loading.setVisibility(View.GONE);
                M_ujian anggotaResponse = response.body();

                if (anggotaResponse.getData().size() > 0){
                    recyclerView.setVisibility(View.VISIBLE);
                    list_anggota = anggotaResponse.getData();
                    anggotaAdapter = new UjianAdapter();
                    anggotaAdapter.addAnggota(list_anggota);
                    recyclerView.setAdapter(anggotaAdapter);
                }else{
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_ujian> call, Throwable t) {
                Log.e("failur",t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                show_form_add();
                break;
            case R.id.btn_reload_rv:
                getUjian();
                break;
        }
    }

    public void show_form_add() {
        final String emailPattern = getString(R.string.email_pattern);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_add_ujian, null);
        final Button select_date = dialogView.findViewById(R.id.btn_select_date);
        final TextInputLayout til_tgl = dialogView.findViewById(R.id.textInputLayoutTgl);
        final TextInputLayout til_nama = dialogView.findViewById(R.id.textInputLayoutNama);
        final TextInputLayout til_ket = dialogView.findViewById(R.id.textInputLayoutKet);
        final TextInputLayout til_waktu = dialogView.findViewById(R.id.textInputLayoutWaktu);
        final TextInputEditText ed_tgl = dialogView.findViewById(R.id.tgl_ujian);
        final TextInputEditText ed_nama = dialogView.findViewById(R.id.nama);
        final TextInputEditText ed_ket = dialogView.findViewById(R.id.ed_ket);
        final TextInputEditText ed_waktu = dialogView.findViewById(R.id.ed_waktu);
        final Button btn_add = dialogView.findViewById(R.id.btn_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
                        til_nama.setError("Nama Ujian Kosong");
                        til_ket.setError(null);
                        til_tgl.setError(null);
                        til_waktu.setError(null);
                    }else if (Objects.requireNonNull(ed_tgl.getText()).toString().isEmpty()) {
                        til_tgl.setError("Tanggal Ujian Kosong");
                        til_ket.setError(null);
                        til_nama.setError(null);
                        til_waktu.setError(null);
                    }else if (Objects.requireNonNull(ed_waktu.getText()).toString().isEmpty()) {
                        til_waktu.setError("Waktu Ujian Kosong");
                        til_ket.setError(null);
                        til_nama.setError(null);
                        til_tgl.setError(null);
                    } else {
                        til_nama.setError(null);
                        til_ket.setError(null);
                        til_tgl.setError(null);
                        til_waktu.setError(null);
                        add_mp(ed_nama.getText().toString().trim(),ed_tgl.getText().toString().trim(),ed_ket.getText().toString().trim(),ed_waktu.getText().toString().trim());
                    }
                }
            }
        });

        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(DataUjianActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        ed_tgl.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });
        dialogBuilder.setView(dialogView);
        alertDialog =dialogBuilder.create();
        alertDialog.show();
    }

    private void add_mp(String nama, String tgl, String ket, String waktu) {
        M.showLoadingDialog(this);
        Call<M_add_data> call = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
            call = RetrofitClient.getInstance().getApi().addUjian(nama,tgl,ket,waktu);
        assert call != null;
        call.enqueue(new Callback<M_add_data>() {
            @Override
            public void onResponse(Call<M_add_data> call, retrofit2.Response<M_add_data> response) {
                M.hideLoadingDialog();
                M_add_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getStatus()){
                    new SweetAlertDialog(DataUjianActivity.this,SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                    alertDialog.dismiss();
                    ll_loading.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    ll_no_data.setVisibility(View.GONE);
                    ll_error.setVisibility(View.GONE);
                    getUjian();
                }else{
                    new SweetAlertDialog(DataUjianActivity.this,SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<M_add_data> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
            }
        });
    }
}