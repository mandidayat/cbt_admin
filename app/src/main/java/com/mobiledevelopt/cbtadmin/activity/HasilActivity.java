package com.mobiledevelopt.cbtadmin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.hasilAdapter;
import com.mobiledevelopt.cbtadmin.model.hasil.M_data_hasil;
import com.mobiledevelopt.cbtadmin.model.hasil.M_hasil;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.mobiledevelopt.cbtadmin.util.Session_management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import retrofit2.Call;
import retrofit2.Callback;

public class HasilActivity extends AppCompatActivity implements View.OnClickListener {

    private HashMap<String, String> user;
    private LinearLayout ll_error,ll_loading,ll_no_data;
    private List<M_data_hasil> list_anggota = new ArrayList<>();
    private static final String[][] DATA_TO_SHOW = { { "This", "is", "a" },
            { "and", "a", "second" } };
    private static final String[] TABLE_HEADERS = { "NISN", "Nama", "Nilai" };
    private  TableView tableView;
    private String id_ujian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        id_ujian= extras.getString("id");
        initial();
    }

    private void initial() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Session_management sessionManagement = new Session_management(HasilActivity.this);
        user = sessionManagement.getUserDetails();
        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        Button reload = findViewById(R.id.btn_reload_rv);
        tableView = findViewById(R.id.tableView);
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, TABLE_HEADERS));

        reload.setOnClickListener(this);
        get_hasil();
    }

    private void get_hasil() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_hasil> call = null;
        call = RetrofitClient.getInstance().getApi().hasilUjian(id_ujian);
        call.enqueue(new Callback<M_hasil>() {
            @Override
            public void onResponse(Call<M_hasil> call, retrofit2.Response<M_hasil> response) {
                ll_loading.setVisibility(View.GONE);
                M_hasil anggotaResponse = response.body();

                if (anggotaResponse.getData().size() > 0){
                    hasilAdapter carTableDataAdapter = new hasilAdapter(HasilActivity.this, anggotaResponse.getData(), tableView);
                    tableView.setDataAdapter(carTableDataAdapter);
//                    list_anggota = anggotaResponse.getData();
//                    anggotaAdapter = new UjianAdapter();
//                    anggotaAdapter.addAnggota(list_anggota);
//                    recyclerView.setAdapter(anggotaAdapter);
                }else{
                    ll_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<M_hasil> call, Throwable t) {
                Log.e("failur",t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view) {

    }
}