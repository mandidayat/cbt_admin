package com.mobiledevelopt.cbtadmin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.adapter.SoalAdapter;
import com.mobiledevelopt.cbtadmin.api.Const;
import com.mobiledevelopt.cbtadmin.model.M_add_data;
import com.mobiledevelopt.cbtadmin.model.M_update_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.soal.M_data_soal;
import com.mobiledevelopt.cbtadmin.model.soal.M_soal;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.vincent.filepicker.Constant.MAX_NUMBER;
import static com.vincent.filepicker.Constant.REQUEST_CODE_PICK_IMAGE;
import static com.vincent.filepicker.Constant.RESULT_PICK_IMAGE;

public class EditSoalActivity extends AppCompatActivity {
    private String id;
    private M_data_soal m_data_soal;
    private String id_ujian,id_mapel;
    private ImageView iv_soal;
    private File file;
    private Spinner List_jawaban,List_mp;
    private List<M_data_mp> list_mp = new ArrayList<>();
    private TextInputEditText ed_soal, ed_pila, ed_pilb, ed_pilc, ed_pild;
    private ImageView iv_gambar;
    private LinearLayout ll_error,ll_loading,ll_no_data,ll_content;
    private Bitmap bitmap_soal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_soal);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button reload = findViewById(R.id.btn_reload_rv);
        List_jawaban = findViewById(R.id.spinner_jawaban);
        List_mp = findViewById(R.id.spinner_mp);
        Button select_image = findViewById(R.id.btn_select_img);
        iv_soal = findViewById(R.id.iv_soal);
        ll_loading = findViewById(R.id.ll_loading_rv);
        ll_error = findViewById(R.id.ll_error_rv);
        ll_no_data = findViewById(R.id.ll_no_data_rv);
        ll_content = findViewById(R.id.ll_content);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSoal();
            }
        });
        file = null;
//        Toast.makeText(this, List_jawaban.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

        List_mp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
//                id_mapel = list_mp.get(position).getIdMapel();
//                Toast.makeText(EditSoalActivity.this, m_data_soal.getMapel(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        final TextInputLayout til_soal = findViewById(R.id.textInputLayoutSoal);
        final TextInputLayout til_pila = findViewById(R.id.textInputLayoutPilA);
        final TextInputLayout til_pilb = findViewById(R.id.textInputLayoutPilB);
        final TextInputLayout til_pilc = findViewById(R.id.textInputLayoutPilC);
        final TextInputLayout til_pild = findViewById(R.id.textInputLayoutPilD);

         ed_soal = findViewById(R.id.ed_soal);
         ed_pila = findViewById(R.id.ed_pila);
         ed_pilb = findViewById(R.id.ed_pilb);
         ed_pilc = findViewById(R.id.ed_pilc);
         ed_pild = findViewById(R.id.ed_pild);
         iv_gambar = findViewById(R.id.iv_soal);

        final Button btn_add = findViewById(R.id.btn_add);

        getSoal();

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditSoalActivity.this, ImagePickActivity.class);
                intent.putExtra(MAX_NUMBER,1);
                startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_soal.getText()).toString().isEmpty()) {
                        til_soal.setError("Soal Kosong");
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pila.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError("Pilihan A Kosong");
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pilb.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError("Pilihan B Kosong");
                        til_pilc.setError(null);
                        til_pild.setError(null);
                    } else if (Objects.requireNonNull(ed_pilc.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError("Pilihan C Kosong");
                        til_pild.setError(null);
                    }else if (Objects.requireNonNull(ed_pild.getText()).toString().isEmpty()) {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError("Pilijan D Kosong");
                    } else {
                        til_soal.setError(null);
                        til_pila.setError(null);
                        til_pilb.setError(null);
                        til_pilc.setError(null);
                        til_pild.setError(null);
                        update_users(ed_soal.getText().toString().trim(), ed_pila.getText().toString().trim(), ed_pilb.getText().toString().trim(), ed_pilc.getText().toString().trim(), ed_pild.getText().toString().trim()
                                , m_data_soal.getIdUjian(), m_data_soal.getIdMapel(),List_jawaban.getSelectedItem().toString(),m_data_soal.getIdSoal());
                    }
                }
            }
        });



    }

    private void update_users(String soal, String pila, String pilb, String pilc, String pild, String id_ujian, String id_mapel, String jawaban, String id_soal) {
        M.showLoadingDialog(this);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("id_soal", createPartFromString(id_soal));
        map.put("id_ujian", createPartFromString(id_ujian));
        map.put("id_mapel", createPartFromString(id_mapel));
        map.put("soal", createPartFromString(soal));
        if(jawaban.equals("A")){
            map.put("jawaban", createPartFromString("1"));
        }else if(jawaban.equals("B")){
            map.put("jawaban", createPartFromString("2"));
        }else if(jawaban.equals("C")){
            map.put("jawaban", createPartFromString("3"));
        }else if(jawaban.equals("D")){
            map.put("jawaban", createPartFromString("4"));
        }
        map.put("pil1", createPartFromString(pila));
        map.put("pil2", createPartFromString(pilb));
        map.put("pil3", createPartFromString(pilc));
        map.put("pil4", createPartFromString(pild));
        //convert gambar jadi File terlebih dahulu dengan memanggil createTempFile yang di atas tadi.
//        File file = createTempFile(bitmap_soal);
        MultipartBody.Part body;

        if (file != null){
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("gambar", file.getName(), reqFile);
        }else{
            body = null;
        }

        Call<M_update_data> call = null;
        call = RetrofitClient.getInstance().getApi().updateSoal(body,map);
        assert call != null;
        call.enqueue(new Callback<M_update_data>() {
            @Override
            public void onResponse(Call<M_update_data> call, retrofit2.Response<M_update_data> response) {
                M.hideLoadingDialog();
                M_update_data loginResponse = response.body();
                assert loginResponse != null;
                if (loginResponse.getMessage().equals("Data Berhasil Di Update")){
                    new SweetAlertDialog(EditSoalActivity.this,SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
//                    Intent intent = new Intent(EditSoalActivity.this,SoalActivity.class);
//                    startActivity(intent);
                }else{
                    new SweetAlertDialog(EditSoalActivity.this,SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(loginResponse.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<M_update_data> call, Throwable t) {
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMp(final String mapel) {
        Call<M_mp> call = null;
        call = RetrofitClient.getInstance().getApi().mp();
        call.enqueue(new Callback<M_mp>() {
            @Override
            public void onResponse(Call<M_mp> call, retrofit2.Response<M_mp> response) {
                M_mp anggotaResponse = response.body();
                if (anggotaResponse.getData().size() > 0){
                    list_mp = anggotaResponse.getData();
                    List<String> listSpinner = new ArrayList<>();
                    for (int i = 0; i < list_mp.size(); i++){
                        listSpinner.add(list_mp.get(i).getMapel());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(EditSoalActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, listSpinner);
                    List_mp.setAdapter(adapter);
                    int spinnerPosition = adapter.getPosition(mapel);
                    List_mp.setSelection(spinnerPosition);
                }
            }

            @Override
            public void onFailure(Call<M_mp> call, Throwable t) {
                Log.e("failur",t.getMessage());
            }
        });
    }

    private void getSoal() {
        ll_loading.setVisibility(View.VISIBLE);
        ll_error.setVisibility(View.GONE);
        Call<M_soal> call = null;
        call = RetrofitClient.getInstance().getApi().soalFilter(id);
        call.enqueue(new Callback<M_soal>() {
            @Override
            public void onResponse(Call<M_soal> call, retrofit2.Response<M_soal> response) {
                if (response.body().getData().size() > 0) {
                    m_data_soal = response.body().getData().get(0);
                    ll_loading.setVisibility(View.GONE);
                    ll_content.setVisibility(View.VISIBLE);
                    List<String> listSpinnerJawaban = new ArrayList<>();
                    listSpinnerJawaban.add("A");
                    listSpinnerJawaban.add("B");
                    listSpinnerJawaban.add("C");
                    listSpinnerJawaban.add("D");

                     ArrayAdapter<String> adapterJawaban = new ArrayAdapter<>(EditSoalActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, listSpinnerJawaban);
                    List_jawaban.setAdapter(adapterJawaban);

                    int spinnerPosition1 = adapterJawaban.getPosition(m_data_soal.getJawaban());
                    List_jawaban.setSelection(spinnerPosition1);
                    getMp(m_data_soal.getMapel());
                    ed_soal.setText(m_data_soal.getSoal());
                    ed_pila.setText(m_data_soal.getPil1());
                    ed_pilb.setText(m_data_soal.getPil2());
                    ed_pilc.setText(m_data_soal.getPil3());
                    ed_pild.setText(m_data_soal.getPil4());

                    if (m_data_soal.getGambar() == null){
                        iv_gambar.setVisibility(View.GONE);
                    }else {
                        iv_gambar.setVisibility(View.VISIBLE);
                        Glide.with(EditSoalActivity.this).load(Const.BASE_URL_Image+m_data_soal.getGambar()).into(iv_gambar);
                    }
                }
            }

            @Override
            public void onFailure(Call<M_soal> call, Throwable t) {
                Log.e("failur", t.getMessage());
                ll_loading.setVisibility(View.GONE);
                ll_error.setVisibility(View.VISIBLE);
                ll_content.setVisibility(View.GONE);
            }
        });
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK && data != null){
            //TODO: action
            ArrayList<ImageFile> pickedImg = data.getParcelableArrayListExtra(RESULT_PICK_IMAGE);
            StringBuilder builder = new StringBuilder();
            for (ImageFile file : pickedImg) {
                String path = file.getPath();
                builder.append(path);
            }

            file = new File(builder.toString());
            Toast.makeText(this, file.getName(), Toast.LENGTH_SHORT).show();
//            Glide.with(this).load(builder.toString()).into(iv_soal);

            Glide.with(this)
                    .asBitmap()
                    .load(builder.toString())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            bitmap_soal = resource;
                            iv_soal.setImageBitmap(resource);
                        }
                    });

        }
    }
}