package com.mobiledevelopt.cbtadmin.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.karan.churi.PermissionManager.PermissionManager;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.util.Session_management;

import androidx.cardview.widget.CardView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    private Session_management sessionManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sessionManagement = new Session_management(MainActivity.this);
        HashMap<String, String> user = sessionManagement.getUserDetails();

        if (!sessionManagement.isLoggedIn()){
            Intent loginsucces = new Intent(this, LoginActivity.class);
            loginsucces.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            loginsucces.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginsucces);
        }else{
            init();
        }

    }

    private void init() {
        CardView card_data_users = findViewById(R.id.card_data_users);
        CardView card_data_pelajaran = findViewById(R.id.card_data_pelajaran);
        CardView data_ujian = findViewById(R.id.data_ujian);
        CardView card_exit = findViewById(R.id.log_out);
        CardView card_bank_soal = findViewById(R.id.bank_soal);

        card_data_users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, UsersActivity.class);
                startActivity(intent);
            }
        });

        card_data_pelajaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DataPelajaranActivity.class);
                startActivity(intent);
            }
        });

        data_ujian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DataUjianActivity.class);
                startActivity(intent);
            }
        });

        card_bank_soal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BankSoalActivity.class);
                startActivity(intent);
            }
        });

        card_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Yakin Ingin Keluar Akun.?")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                sessionManagement.logoutSession();
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setCancelButton("Batal", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

}