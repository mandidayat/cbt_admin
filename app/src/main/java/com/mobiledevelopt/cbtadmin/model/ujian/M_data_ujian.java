package com.mobiledevelopt.cbtadmin.model.ujian;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_data_ujian {
    @SerializedName("id_ujian")
    @Expose
    private String idUjian;
    @SerializedName("nama_ujian")
    @Expose
    private String namaUjian;
    @SerializedName("tgl_ujian")
    @Expose
    private String tglUjian;
    @SerializedName("ket")
    @Expose
    private String ket;
    @SerializedName("stat")
    @Expose
    private String stat;
    @SerializedName("waktu")
    @Expose
    private String waktu;

    public String getIdUjian() {
        return idUjian;
    }

    public void setIdUjian(String idUjian) {
        this.idUjian = idUjian;
    }

    public String getNamaUjian() {
        return namaUjian;
    }

    public void setNamaUjian(String namaUjian) {
        this.namaUjian = namaUjian;
    }

    public String getTglUjian() {
        return tglUjian;
    }

    public void setTglUjian(String tglUjian) {
        this.tglUjian = tglUjian;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }
}
