package com.mobiledevelopt.cbtadmin.model.hasil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mobiledevelopt.cbtadmin.model.ujian.M_data_ujian;

import java.util.List;

public class M_hasil {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<M_data_hasil> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<M_data_hasil> getData() {
        return data;
    }

    public void setData(List<M_data_hasil> data) {
        this.data = data;
    }
}
