package com.mobiledevelopt.cbtadmin.model.peserta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_data_peserta {
    @SerializedName("id_peserta")
    @Expose
    private String id_peserta;
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("tgl_lahir")
    @Expose
    private String tglLahir;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("ayah")
    @Expose
    private String ayah;
    @SerializedName("ibu")
    @Expose
    private String ibu;
    @SerializedName("wali")
    @Expose
    private String wali;
    @SerializedName("id_ujian")
    @Expose
    private String idUjian;
    @SerializedName("password")
    @Expose
    private String password;

    public String getId_peserta() {
        return id_peserta;
    }

    public void setId_peserta(String id_peserta) {
        this.id_peserta = id_peserta;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getAyah() {
        return ayah;
    }

    public void setAyah(String ayah) {
        this.ayah = ayah;
    }

    public String getIbu() {
        return ibu;
    }

    public void setIbu(String ibu) {
        this.ibu = ibu;
    }

    public String getWali() {
        return wali;
    }

    public void setWali(String wali) {
        this.wali = wali;
    }

    public String getIdUjian() {
        return idUjian;
    }

    public void setIdUjian(String idUjian) {
        this.idUjian = idUjian;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
