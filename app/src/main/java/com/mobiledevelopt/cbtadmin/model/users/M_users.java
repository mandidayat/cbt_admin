package com.mobiledevelopt.cbtadmin.model.users;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_users {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<M_data_users> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<M_data_users> getData() {
        return data;
    }

    public void setData(List<M_data_users> data) {
        this.data = data;
    }
}
