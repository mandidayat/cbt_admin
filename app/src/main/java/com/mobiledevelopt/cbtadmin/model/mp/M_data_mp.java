package com.mobiledevelopt.cbtadmin.model.mp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_data_mp {
    @SerializedName("id_mapel")
    @Expose
    private String idMapel;
    @SerializedName("mapel")
    @Expose
    private String mapel;

    public String getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(String idMapel) {
        this.idMapel = idMapel;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }
}
