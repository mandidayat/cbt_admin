package com.mobiledevelopt.cbtadmin.model.kuis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_kuis_data {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("soal")
    @Expose
    private String soal;
    @SerializedName("a")
    @Expose
    private String a;
    @SerializedName("b")
    @Expose
    private String b;
    @SerializedName("c")
    @Expose
    private String c;
    @SerializedName("jwaban")
    @Expose
    private String jwaban;
    @SerializedName("gambar")
    @Expose
    private String gambar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSoal() {
        return soal;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getJwaban() {
        return jwaban;
    }

    public void setJwaban(String jwaban) {
        this.jwaban = jwaban;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
