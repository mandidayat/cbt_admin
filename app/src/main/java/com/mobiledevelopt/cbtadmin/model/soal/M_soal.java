package com.mobiledevelopt.cbtadmin.model.soal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class M_soal {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<M_data_soal> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<M_data_soal> getData() {
        return data;
    }

    public void setData(List<M_data_soal> data) {
        this.data = data;
    }
}
