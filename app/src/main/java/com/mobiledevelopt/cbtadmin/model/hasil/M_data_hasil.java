package com.mobiledevelopt.cbtadmin.model.hasil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class M_data_hasil {
    @SerializedName("id_hasil")
    @Expose
    private String idHasil;
    @SerializedName("id_peserta")
    @Expose
    private String idPeserta;
    @SerializedName("id_ujian")
    @Expose
    private String idUjian;
    @SerializedName("nilai")
    @Expose
    private String nilai;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("tgl_ujian")
    @Expose
    private String tgl_ujian;
    @SerializedName("nama_ujian")
    @Expose
    private String nama_ujian;

    public String getIdHasil() {
        return idHasil;
    }

    public void setIdHasil(String idHasil) {
        this.idHasil = idHasil;
    }

    public String getIdPeserta() {
        return idPeserta;
    }

    public void setIdPeserta(String idPeserta) {
        this.idPeserta = idPeserta;
    }

    public String getIdUjian() {
        return idUjian;
    }

    public void setIdUjian(String idUjian) {
        this.idUjian = idUjian;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getTgl_ujian() {
        return tgl_ujian;
    }

    public void setTgl_ujian(String tgl_ujian) {
        this.tgl_ujian = tgl_ujian;
    }

    public String getNama_ujian() {
        return nama_ujian;
    }

    public void setNama_ujian(String nama_ujian) {
        this.nama_ujian = nama_ujian;
    }
}
