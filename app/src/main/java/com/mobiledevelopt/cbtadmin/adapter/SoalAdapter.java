package com.mobiledevelopt.cbtadmin.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.activity.EditSoalActivity;
import com.mobiledevelopt.cbtadmin.activity.SoalActivity;
import com.mobiledevelopt.cbtadmin.api.Const;
import com.mobiledevelopt.cbtadmin.model.M_delete_data;
import com.mobiledevelopt.cbtadmin.model.M_update_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.model.mp.M_mp;
import com.mobiledevelopt.cbtadmin.model.soal.M_data_soal;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;
import com.vincent.filepicker.activity.ImagePickActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

import static com.vincent.filepicker.Constant.MAX_NUMBER;
import static com.vincent.filepicker.Constant.REQUEST_CODE_PICK_IMAGE;

public class SoalAdapter extends RecyclerView.Adapter<SoalAdapter.MyViewHolder> {

    private List<M_data_soal> layanan;
    private Context context;
    AlertDialog alertDialog;
    private List<M_data_mp> list_mp = new ArrayList<>();
    Spinner List_mp;
    private ImageView iv_soal;
    private Bitmap bitmap_soal;
    private File file;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_soal,tv_no,tv_mapel;
        private RadioButton radio0,radio1,radio2,radio3;
        private ImageView iv_gambar;
        private Button btn_delete,btn_edit;

        MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tv_no = view.findViewById(R.id.tv_no);
            tv_soal = view.findViewById(R.id.tv_soal);
            tv_mapel = view.findViewById(R.id.tv_mapel);
            iv_gambar = view.findViewById(R.id.iv_gambar);
            radio0 = view.findViewById(R.id.radio0);
            radio1 = view.findViewById(R.id.radio1);
            radio2 = view.findViewById(R.id.radio2);
            radio3 = view.findViewById(R.id.radio3);

            btn_delete = view.findViewById(R.id.btn_delete);
            btn_edit = view.findViewById(R.id.btn_edit);
        }
    }

    public void addAnggota(List<M_data_soal> layanan) {
        this.layanan = layanan;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_soal, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final M_data_soal model = layanan.get(position);

        holder.tv_no.setText("Soal No. "+ String.valueOf(position+1));
        holder.tv_soal.setText(model.getSoal());
        holder.radio0.setText(model.getPil1());
        holder.radio1.setText(model.getPil2());
        holder.radio2.setText(model.getPil3());
        holder.radio3.setText(model.getPil4());
        holder.tv_mapel.setText("Mapel : "+ model.getMapel());

        if (model.getGambar() == null){
            holder.iv_gambar.setVisibility(View.GONE);
        }else {
            holder.iv_gambar.setVisibility(View.VISIBLE);
            Glide.with(context).load(Const.BASE_URL_Image+model.getGambar()).into(holder.iv_gambar);
        }

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDelete(model.getIdSoal(),position);
            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                show_form_edit(model,position);
                Intent intent = new Intent(context, EditSoalActivity.class);
                intent.putExtra("id",model.getIdSoal());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return layanan.size();
    }

    public void setFilter(ArrayList<M_data_soal> filterList){
        layanan = new ArrayList<>();
        layanan.addAll(filterList);
        notifyDataSetChanged();
    }

    private void showDialogDelete(final String id, final int position) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Data yang dihapus tidak dapat dikembalikan");
        alertbox.setTitle("Warning");
        alertbox.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        M.showLoadingDialog(context);
                        Call<M_delete_data> call = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                            call = RetrofitClient.getInstance().getApi().deleteSoal(id);
                        assert call != null;
                        call.enqueue(new Callback<M_delete_data>() {
                            @Override
                            public void onResponse(Call<M_delete_data> call, retrofit2.Response<M_delete_data> response) {
                                M.hideLoadingDialog();
                                M_delete_data loginResponse = response.body();
                                assert loginResponse != null;
                                if (loginResponse.getMessage().equals("Data Berhasil Di Hapus")){
                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                    deleteData(position);
                                }else{
                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                }

                            }

                            @Override
                            public void onFailure(Call<M_delete_data> call, Throwable t) {
                                M.hideLoadingDialog();
                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertbox.show();
    }

    private void deleteData(int position) {
        layanan.remove(position);
        notifyDataSetChanged();
    }

    public void show_form_edit(final M_data_soal model, final int position) {

        final String emailPattern = context.getString(R.string.email_pattern);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_edit_soal, null);

        final Spinner List_jawaban = dialogView.findViewById(R.id.spinner_jawaban);
        List_mp = dialogView.findViewById(R.id.spinner_mp);
        final Button select_image = dialogView.findViewById(R.id.btn_select_img);
        iv_soal = dialogView.findViewById(R.id.iv_soal);

        file = null;
//        Toast.makeText(this, List_jawaban.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

        List_mp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
//                id_mapel = list_mp.get(position).getIdMapel();
                Toast.makeText(context, list_mp.get(position).getMapel(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        final TextInputLayout til_soal = dialogView.findViewById(R.id.textInputLayoutSoal);
        final TextInputLayout til_pila = dialogView.findViewById(R.id.textInputLayoutPilA);
        final TextInputLayout til_pilb = dialogView.findViewById(R.id.textInputLayoutPilB);
        final TextInputLayout til_pilc = dialogView.findViewById(R.id.textInputLayoutPilC);
        final TextInputLayout til_pild = dialogView.findViewById(R.id.textInputLayoutPilD);

        final TextInputEditText ed_soal = dialogView.findViewById(R.id.ed_soal);
        final TextInputEditText ed_pila = dialogView.findViewById(R.id.ed_pila);
        final TextInputEditText ed_pilb = dialogView.findViewById(R.id.ed_pilb);
        final TextInputEditText ed_pilc = dialogView.findViewById(R.id.ed_pilc);
        final TextInputEditText ed_pild = dialogView.findViewById(R.id.ed_pild);

        ed_soal.setText(model.getSoal());
        ed_pila.setText(model.getPil1());
        ed_pilb.setText(model.getPil2());
        ed_pilc.setText(model.getPil3());
        ed_pild.setText(model.getPil4());

        final Button btn_add = dialogView.findViewById(R.id.btn_add);

        List<String> listSpinnerJawaban = new ArrayList<>();
        listSpinnerJawaban.add("A");
        listSpinnerJawaban.add("B");
        listSpinnerJawaban.add("C");
        listSpinnerJawaban.add("D");

        ArrayAdapter<String> adapterJawaban = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, listSpinnerJawaban);
        List_jawaban.setAdapter(adapterJawaban);
        int spinnerPosition1 = adapterJawaban.getPosition(model.getJawaban());
        List_jawaban.setSelection(spinnerPosition1);

        getMp(model.getMapel());

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImagePickActivity.class);
                intent.putExtra(MAX_NUMBER,1);
                ((Activity) context).startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
            }
        });

//        btn_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Objects.requireNonNull(ed_nisn.getText()).toString().isEmpty()) {
//                    til_nisn.setError("NISN Kosong");
//                    til_nisn.setError(null);
//                    til_tgl_lahir.setError(null);
//                    til_password.setError(null);
//                } else if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
//                    til_nama.setError("Nama Kosong");
//                    til_nama.setError(null);
//                    til_tgl_lahir.setError(null);
//                    til_password.setError(null);
//                } else if (Objects.requireNonNull(ed_tgl_lahir.getText()).toString().isEmpty()) {
//                    til_tgl_lahir.setError("Tanggal Lahir Kosong");
//                    til_nisn.setError(null);
//                    til_nama.setError(null);
//                    til_password.setError(null);
//                }  else {
//                    til_tgl_lahir.setError(null);
//                    til_nisn.setError(null);
//                    til_nama.setError(null);
//                    til_password.setError(null);
//                    edit_mp(ed_nisn.getText().toString().trim(), ed_nama.getText().toString().trim(), ed_tgl_lahir.getText().toString().trim(), ed_alamat.getText().toString().trim(), ed_no_hp.getText().toString().trim()
//                            , ed_ayah.getText().toString().trim(), ed_ibu.getText().toString().trim(), ed_wali.getText().toString().trim(), model.getIdUjian(), ed_password.getText().toString().trim(),position);
//                }
//            }
//        });

        dialogBuilder.setView(dialogView);
        alertDialog =dialogBuilder.create();
        alertDialog.show();
    }

    private void getMp(final String mapel) {
        Call<M_mp> call = null;
        call = RetrofitClient.getInstance().getApi().mp();
        call.enqueue(new Callback<M_mp>() {
            @Override
            public void onResponse(Call<M_mp> call, retrofit2.Response<M_mp> response) {
                M_mp anggotaResponse = response.body();
                if (anggotaResponse.getData().size() > 0){
                    list_mp = anggotaResponse.getData();
                    List<String> listSpinner = new ArrayList<>();
                    for (int i = 0; i < list_mp.size(); i++){
                        listSpinner.add(list_mp.get(i).getMapel());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                            android.R.layout.simple_spinner_dropdown_item, listSpinner);
                    List_mp.setAdapter(adapter);
                    int spinnerPosition = adapter.getPosition(mapel);
                    List_mp.setSelection(spinnerPosition);
                }
            }

            @Override
            public void onFailure(Call<M_mp> call, Throwable t) {
                Log.e("failur",t.getMessage());
            }
        });
    }

//    private void edit_mp(final String nisn, final String nama, final String tgl_lahir, final String alamat, final String no_hp, final String ayah, final String ibu, final String wali, final String id_ujian, final String password, final int position) {
//        final AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
//        alertbox.setMessage("Yakin ingin mengedit data");
//        alertbox.setTitle("Warning");
//        alertbox.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface arg0,
//                                        int arg1) {
//                        M.showLoadingDialog(context);
//                        Call<M_update_data> call = null;
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
//                            call = RetrofitClient.getInstance().getApi().updatePeserta(nisn, nama,tgl_lahir, alamat,no_hp,ayah,ibu,wali,id_ujian,password);
//                        assert call != null;
//                        call.enqueue(new Callback<M_update_data>() {
//                            @Override
//                            public void onResponse(Call<M_update_data> call, retrofit2.Response<M_update_data> response) {
//                                M.hideLoadingDialog();
//                                alertDialog.dismiss();
//                                M_update_data loginResponse = response.body();
//                                assert loginResponse != null;
//                                if (loginResponse.getMessage().equals("Data Berhasil Di Update")){
//                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
//                                            .setTitleText(loginResponse.getMessage())
//                                            .show();
//                                    updateData(nisn, nama,tgl_lahir, alamat,no_hp,ayah,ibu,wali,id_ujian,password,position);
//                                }else{
//                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
//                                            .setTitleText(loginResponse.getMessage())
//                                            .show();
//                                }
//                            }
//
//                            @Override
//                            public void onFailure(Call<M_update_data> call, Throwable t) {
//                                M.hideLoadingDialog();
//                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//                });
//        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//
//            }
//        });
//        alertbox.show();
//    }

    private void updateData(final String nisn, final String nama, final String tgl_lahir, final String alamat, final String no_hp, final String ayah, final String ibu, final String wali, final String id_ujian, final String password, final int position) {
        M_data_soal model = new M_data_soal();
//        model.setNisn(nisn);
//        model.setNama(nama);
//        model.setTglLahir(tgl_lahir);
//        model.setAlamat(alamat);
//        model.setNoHp(no_hp);
//        model.setAyah(ayah);
//        model.setIbu(ibu);
//        model.setWali(wali);
//        model.setPassword(password);
        layanan.set(position,model);
        notifyDataSetChanged();
    }



}
