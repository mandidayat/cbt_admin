package com.mobiledevelopt.cbtadmin.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.model.M_delete_data;
import com.mobiledevelopt.cbtadmin.model.M_update_data;
import com.mobiledevelopt.cbtadmin.model.mp.M_data_mp;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class MPAdapter extends RecyclerView.Adapter<MPAdapter.MyViewHolder> {

    private List<M_data_mp> layanan;
    private Context context;
    AlertDialog alertDialog;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_nama,tv_id;
        private Button btn_delete,btn_edit;

        MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tv_nama = view.findViewById(R.id.tv_nama_ujian);
            tv_id = view.findViewById(R.id.tv_id_ujian);
            btn_delete = view.findViewById(R.id.btn_delete);
            btn_edit = view.findViewById(R.id.btn_edit);
        }
    }

    public void addAnggota(List<M_data_mp> layanan) {
        this.layanan = layanan;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pelajaran, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final M_data_mp model = layanan.get(position);

        holder.tv_nama.setText(model.getMapel());
        holder.tv_id.setText(model.getIdMapel());
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDelete(model.getIdMapel(),position);
            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_form_edit(model,position);
            }
        });
    }

    private void showDialogDelete(final String idMapel, final int position) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Data yang dihapus tidak dapat dikembalikan");
        alertbox.setTitle("Warning");
        alertbox.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        M.showLoadingDialog(context);
                        Call<M_delete_data> call = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
                            call = RetrofitClient.getInstance().getApi().deleteMp(idMapel);
                        assert call != null;
                        call.enqueue(new Callback<M_delete_data>() {
                            @Override
                            public void onResponse(Call<M_delete_data> call, retrofit2.Response<M_delete_data> response) {
                                M.hideLoadingDialog();
                                M_delete_data loginResponse = response.body();
                                assert loginResponse != null;
                                if (loginResponse.getMessage().equals("Data Berhasil Di Hapus")){
                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                    deleteData(position);
                                }else{
                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                }

                            }

                            @Override
                            public void onFailure(Call<M_delete_data> call, Throwable t) {
                                M.hideLoadingDialog();
                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertbox.show();
    }

    private void deleteData(int position) {
        layanan.remove(position);
        notifyDataSetChanged();
    }

    public void show_form_edit(final M_data_mp model, final int position) {

        final String emailPattern = context.getString(R.string.email_pattern);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_edit_mp, null);

        final TextInputLayout til_nama = dialogView.findViewById(R.id.textInputLayoutNama);
        final TextInputEditText ed_nama = dialogView.findViewById(R.id.nama);
        final Button btn_add = dialogView.findViewById(R.id.btn_add);
        ed_nama.setText(model.getMapel());
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
                        til_nama.setError("Nama Mata Pelajaran Kosong");
                    } else {
                        til_nama.setError(null);
                        edit_mp(ed_nama.getText().toString().trim(),model.getIdMapel(),position);
                    }
                }
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog =dialogBuilder.create();
        alertDialog.show();
    }

    private void edit_mp(final String mp, final String idMapel, final int position) {
        final AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Yakin ingin mengedit data");
        alertbox.setTitle("Warning");
        alertbox.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        M.showLoadingDialog(context);
                        Call<M_update_data> call = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
                            call = RetrofitClient.getInstance().getApi().updateMp(mp,idMapel);
                        assert call != null;
                        call.enqueue(new Callback<M_update_data>() {
                            @Override
                            public void onResponse(Call<M_update_data> call, retrofit2.Response<M_update_data> response) {
                                M.hideLoadingDialog();
                                alertDialog.dismiss();
                                M_update_data loginResponse = response.body();
                                assert loginResponse != null;
                                if (loginResponse.getMessage().equals("Data Berhasil Di Update")){
                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                    updateData(mp,position,idMapel);
                                }else{
                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                }
                            }

                            @Override
                            public void onFailure(Call<M_update_data> call, Throwable t) {
                                M.hideLoadingDialog();
                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertbox.show();
    }

    private void updateData(String mp, int position, String idMapel) {
        M_data_mp model = new M_data_mp();
        model.setIdMapel(idMapel);
        model.setMapel(mp);
        layanan.set(position,model);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return layanan.size();
    }

    public void setFilter(ArrayList<M_data_mp> filterList){
        layanan = new ArrayList<>();
        layanan.addAll(filterList);
        notifyDataSetChanged();
    }


}
