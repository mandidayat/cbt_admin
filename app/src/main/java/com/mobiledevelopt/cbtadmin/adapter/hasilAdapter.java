package com.mobiledevelopt.cbtadmin.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mobiledevelopt.cbtadmin.model.hasil.M_data_hasil;

import java.text.NumberFormat;
import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.LongPressAwareTableDataAdapter;

public class hasilAdapter extends LongPressAwareTableDataAdapter<M_data_hasil> {

    private static final int TEXT_SIZE = 14;
    private static final NumberFormat PRICE_FORMATTER = NumberFormat.getNumberInstance();


    public hasilAdapter(final Context context, final List<M_data_hasil> data, final TableView<M_data_hasil> tableView) {
        super(context, data, tableView);
    }

    @Override
    public View getDefaultCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final M_data_hasil car = getRowData(rowIndex);
        View renderedView = null;

        switch (columnIndex) {
            case 0:
                renderedView = renderNisn(car);
                break;
            case 1:
                renderedView = renderNama(car);
                break;
            case 2:
                renderedView = renderNilai(car);
                break;
        }

        return renderedView;
    }

    @Override
    public View getLongPressCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        final M_data_hasil car = getRowData(rowIndex);
        View renderedView = null;

//        switch (columnIndex) {
//            case 1:
//                renderedView = renderEditableCatName(car);
//                break;
//            default:
//                renderedView = getDefaultCellView(rowIndex, columnIndex, parentView);
//        }

        return renderedView;
    }

    private View renderCatName(final M_data_hasil car) {
        return renderString(car.getNama());
    }

    private View renderNisn(final M_data_hasil car) {
        return renderString(car.getNisn());
    }

    private View renderNama(final M_data_hasil car) {
        return renderString(car.getNama());
    }

    private View renderNilai(final M_data_hasil car) {
        return renderString(car.getNilai());
    }



    private View renderString(final String value) {
        final TextView textView = new TextView(getContext());
        textView.setText(value);
        textView.setPadding(20, 10, 20, 10);
        textView.setTextSize(TEXT_SIZE);
        return textView;
    }
}
