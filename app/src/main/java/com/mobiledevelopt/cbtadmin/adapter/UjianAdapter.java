package com.mobiledevelopt.cbtadmin.adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobiledevelopt.cbtadmin.R;
import com.mobiledevelopt.cbtadmin.activity.DataUjianActivity;
import com.mobiledevelopt.cbtadmin.activity.HasilActivity;
import com.mobiledevelopt.cbtadmin.activity.PesertaActivity;
import com.mobiledevelopt.cbtadmin.activity.SoalActivity;
import com.mobiledevelopt.cbtadmin.model.M_delete_data;
import com.mobiledevelopt.cbtadmin.model.M_update_data;
import com.mobiledevelopt.cbtadmin.model.ujian.M_data_ujian;
import com.mobiledevelopt.cbtadmin.util.M;
import com.mobiledevelopt.cbtadmin.util.RetrofitClient;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;

public class UjianAdapter extends RecyclerView.Adapter<UjianAdapter.MyViewHolder> {

    private List<M_data_ujian> layanan;
    private Context context;
    AlertDialog alertDialog;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_nama,tv_id,tv_ket,tv_tgl,tv_waktu;
        private Button btn_delete,btn_edit,tbn_soal,btn_peserta,btn_hasil;

        MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            tv_nama = view.findViewById(R.id.tv_nama_ujian);
            tv_id = view.findViewById(R.id.tv_id_ujian);
            tv_tgl = view.findViewById(R.id.tv_tgl);
            tv_ket = view.findViewById(R.id.tv_ket);
            tv_waktu = view.findViewById(R.id.tv_waktu);

            btn_delete = view.findViewById(R.id.btn_delete);
            btn_edit = view.findViewById(R.id.btn_edit);
            tbn_soal = view.findViewById(R.id.btn_soal);
            btn_peserta = view.findViewById(R.id.btn_peserta);
            btn_hasil = view.findViewById(R.id.btn_hasil);
        }
    }

    public void addAnggota(List<M_data_ujian> layanan) {
        this.layanan = layanan;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ujian, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final M_data_ujian model = layanan.get(position);

        holder.tv_tgl.setText(M.convertDate(model.getTglUjian()));
        holder.tv_nama.setText(model.getNamaUjian());
        holder.tv_id.setText(model.getIdUjian());
        holder.tv_ket.setText(model.getKet());
        holder.tv_waktu.setText(model.getWaktu() + " menit");

        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDelete(model.getIdUjian(),position);
            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_form_edit(model,position);
            }
        });
        holder.tbn_soal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SoalActivity.class);
                intent.putExtra("id",model.getIdUjian());
                context.startActivity(intent);
            }
        });
        holder.btn_peserta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PesertaActivity.class);
                intent.putExtra("id",model.getIdUjian());
                context.startActivity(intent);
            }
        });
        holder.btn_hasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HasilActivity.class);
                intent.putExtra("id",model.getIdUjian());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return layanan.size();
    }

    public void setFilter(ArrayList<M_data_ujian> filterList){
        layanan = new ArrayList<>();
        layanan.addAll(filterList);
        notifyDataSetChanged();
    }

    private void showDialogDelete(final String id, final int position) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Data yang dihapus tidak dapat dikembalikan");
        alertbox.setTitle("Warning");
        alertbox.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        M.showLoadingDialog(context);
                        Call<M_delete_data> call = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                            call = RetrofitClient.getInstance().getApi().deleteUjian(id);
                        assert call != null;
                        call.enqueue(new Callback<M_delete_data>() {
                            @Override
                            public void onResponse(Call<M_delete_data> call, retrofit2.Response<M_delete_data> response) {
                                M.hideLoadingDialog();
                                M_delete_data loginResponse = response.body();
                                assert loginResponse != null;
                                if (loginResponse.getMessage().equals("Data Berhasil Di Hapus")){
                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                    deleteData(position);
                                }else{
                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                }

                            }

                            @Override
                            public void onFailure(Call<M_delete_data> call, Throwable t) {
                                M.hideLoadingDialog();
                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertbox.show();
    }

    private void deleteData(int position) {
        layanan.remove(position);
        notifyDataSetChanged();
    }

    public void show_form_edit(final M_data_ujian model, final int position) {

        final String emailPattern = context.getString(R.string.email_pattern);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //this.getLayoutInflater();
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.dialog_edit_ujian, null);

        final TextInputLayout til_nama = dialogView.findViewById(R.id.textInputLayoutNama);
        final TextInputLayout til_tgl = dialogView.findViewById(R.id.textInputLayoutTgl);
        final TextInputLayout til_ket = dialogView.findViewById(R.id.textInputLayoutKet);
        final TextInputLayout til_waktu = dialogView.findViewById(R.id.textInputLayoutWaktu);
        final TextInputEditText ed_nama = dialogView.findViewById(R.id.ed_nama);
        final TextInputEditText ed_tgl = dialogView.findViewById(R.id.ed_tgl);
        final TextInputEditText ed_ket = dialogView.findViewById(R.id.ed_ket);
        final TextInputEditText ed_waktu = dialogView.findViewById(R.id.ed_waktu);
        final Button btn_add = dialogView.findViewById(R.id.btn_add);
        final Button select_date = dialogView.findViewById(R.id.btn_select_date);

        ed_nama.setText(model.getNamaUjian());
        ed_tgl.setText(M.convertDate(model.getTglUjian()));
        ed_ket.setText(model.getKet());
        ed_waktu.setText(model.getWaktu());

        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        ed_tgl.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Objects.requireNonNull(ed_nama.getText()).toString().isEmpty()) {
                    til_nama.setError("Nama Kosong");
                    til_tgl.setError(null);
                    til_ket.setError(null);
                    til_waktu.setError(null);
                } else if (Objects.requireNonNull(ed_tgl.getText()).toString().isEmpty()) {
                    til_tgl.setError("Tanggal Kosong");
                    til_nama.setError(null);
                    til_ket.setError(null);
                    til_waktu.setError(null);
                } else {
                    til_nama.setError(null);
                    til_ket.setError(null);
                    til_tgl.setError(null);
                    til_waktu.setError(null);
                    edit_mp(ed_nama.getText().toString().trim(),ed_tgl.getText().toString().trim(), Objects.requireNonNull(ed_ket.getText()).toString().trim(), Objects.requireNonNull(ed_waktu.getText()).toString().trim(),model.getIdUjian(),position);
                }
            }
        });

        dialogBuilder.setView(dialogView);
        alertDialog =dialogBuilder.create();
        alertDialog.show();
    }

    private void edit_mp(final String nama, final String tgl, final String ket, final String waktu, final String idMapel, final int position) {
        final AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setMessage("Yakin ingin mengedit data");
        alertbox.setTitle("Warning");
        alertbox.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        M.showLoadingDialog(context);
                        Call<M_update_data> call = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                            call = RetrofitClient.getInstance().getApi().updateUjian(nama,tgl,ket,waktu,idMapel);
                        assert call != null;
                        call.enqueue(new Callback<M_update_data>() {
                            @Override
                            public void onResponse(Call<M_update_data> call, retrofit2.Response<M_update_data> response) {
                                M.hideLoadingDialog();
                                alertDialog.dismiss();
                                M_update_data loginResponse = response.body();
                                assert loginResponse != null;
                                if (loginResponse.getMessage().equals("Data Berhasil Di Update")){
                                    new SweetAlertDialog(context,SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                    updateData(nama,tgl,ket,waktu,position,idMapel);
                                }else{
                                    new SweetAlertDialog(context,SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(loginResponse.getMessage())
                                            .show();
                                }
                            }

                            @Override
                            public void onFailure(Call<M_update_data> call, Throwable t) {
                                M.hideLoadingDialog();
                                Toast.makeText(context, "Tidak Dapat Terhubung Keserver. ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
        alertbox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertbox.show();
    }

    private void updateData(String nama,String tgl, String ket,String waktu, int position, String idMapel) {
        M_data_ujian model1 = new M_data_ujian();
        model1.setIdUjian(idMapel);
        model1.setNamaUjian(nama);
        model1.setKet(ket);
        model1.setTglUjian(M.convertDate2(tgl));
        model1.setWaktu(waktu);
        layanan.set(position,model1);
        notifyDataSetChanged();
    }



}
